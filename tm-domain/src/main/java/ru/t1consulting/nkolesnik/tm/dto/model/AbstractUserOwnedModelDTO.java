package ru.t1consulting.nkolesnik.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public class AbstractUserOwnedModelDTO extends AbstractModelDTO {

    @NotNull
    @Column(nullable = false, name = "user_id")
    private String userId;


}
