package ru.t1consulting.nkolesnik.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.model.IWBS;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tasks")
public final class Task extends AbstractWbsModel implements IWBS {

    private static final long serialVersionUID = 1;

    @Nullable
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

}
