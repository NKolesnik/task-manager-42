package ru.t1consulting.nkolesnik.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IAuthEndpoint;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IProjectEndpoint;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IProjectTaskEndpoint;
import ru.t1consulting.nkolesnik.tm.api.endpoint.ITaskEndpoint;
import ru.t1consulting.nkolesnik.tm.dto.model.TaskDTO;
import ru.t1consulting.nkolesnik.tm.dto.request.project.ProjectCreateRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.project.ProjectRemoveByIdRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.task.*;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserLoginRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserLogoutRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserProfileRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.project.ProjectCreateResponse;
import ru.t1consulting.nkolesnik.tm.dto.response.task.TaskBindToProjectResponse;
import ru.t1consulting.nkolesnik.tm.dto.response.task.TaskGetByIdResponse;
import ru.t1consulting.nkolesnik.tm.dto.response.task.TaskUnbindFromProjectResponse;
import ru.t1consulting.nkolesnik.tm.dto.response.user.UserLoginResponse;
import ru.t1consulting.nkolesnik.tm.dto.response.user.UserProfileResponse;
import ru.t1consulting.nkolesnik.tm.marker.SoapCategory;

import java.util.List;
import java.util.UUID;

@Category(SoapCategory.class)
public class ProjectTaskEndpoint {

    @NotNull
    private static final String TASK_NAME = "TEST_TASK_NAME";

    @NotNull
    private static final String TASK_DESCRIPTION = "TEST_TASK_DESCRIPTION";

    @NotNull
    private static final String PROJECT_NAME = "TEST_PROJECT_NAME";

    @NotNull
    private static final String PROJECT_DESCRIPTION = "TEST_PROJECT_DESCRIPTION";

    @NotNull
    private static final String TEST_USER_LOGIN = "test";

    @NotNull
    private static final String TEST_USER_PASSWORD = "test";

    @NotNull
    private static final String ADMIN_USER_LOGIN = "admin";

    @NotNull
    private static final String ADMIN_USER_PASSWORD = "admin";

    @NotNull
    private static final String BAD_TOKEN = UUID.randomUUID().toString();

    @Nullable
    private static final String NULL_TOKEN = null;

    @Nullable
    private static final String NULL_TASK_ID = null;

    @NotNull
    private static final String BAD_TASK_ID = UUID.randomUUID().toString();

    @Nullable
    private static final String NULL_PROJECT_ID = null;


    private static final long COUNT_TEST_TASKS = 10L;

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = IProjectTaskEndpoint.newInstance();

    @Nullable
    private String testToken;

    @Nullable
    private String adminToken;

    @Nullable
    private String testProjectId;

    @Nullable
    private List<TaskDTO> testTasksList;

    @Before
    public void setup() {
        @NotNull final UserLoginRequest testLoginRequest = new UserLoginRequest(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        @NotNull final UserLoginResponse testLoginResponse = authEndpoint.login(testLoginRequest);
        testToken = testLoginResponse.getToken();
        @NotNull final UserProfileRequest testProfileRequest = new UserProfileRequest(testToken);
        @NotNull final UserProfileResponse testProfileResponse = authEndpoint.profile(testProfileRequest);
        @NotNull final UserLoginRequest adminLoginRequest = new UserLoginRequest(ADMIN_USER_LOGIN, ADMIN_USER_PASSWORD);
        @NotNull final UserLoginResponse adminLoginResponse = authEndpoint.login(adminLoginRequest);
        adminToken = adminLoginResponse.getToken();
        @NotNull final UserProfileRequest adminProfileRequest = new UserProfileRequest(adminToken);
        @NotNull final UserProfileResponse adminProfileResponse = authEndpoint.profile(adminProfileRequest);
        @NotNull final TaskListRequest listRequest = new TaskListRequest(testToken);
        createManyTestTasks();
        testProjectId = createTestProject();
        testTasksList = taskEndpoint.listTask(listRequest).getTasks();
    }

    @After
    public void cleanup() {
        @NotNull final UserLogoutRequest testLogoutRequest = new UserLogoutRequest(testToken);
        @NotNull final UserLogoutRequest adminLogoutRequest = new UserLogoutRequest(adminToken);
        @NotNull final TaskClearRequest clearRequest = new TaskClearRequest(testToken);
        taskEndpoint.clearTask(clearRequest);
        removeTestProject(testProjectId);
        authEndpoint.logout(testLogoutRequest);
        authEndpoint.logout(adminLogoutRequest);
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertNotNull(testTasksList);
        @NotNull final TaskDTO task = testTasksList.get(0);
        @NotNull final String taskId = task.getId();
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.bindTaskToProject(new TaskBindToProjectRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.bindTaskToProject(new TaskBindToProjectRequest(NULL_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.bindTaskToProject(new TaskBindToProjectRequest(BAD_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.bindTaskToProject(new TaskBindToProjectRequest(adminToken))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.bindTaskToProject(new TaskBindToProjectRequest(testToken))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.bindTaskToProject(
                        new TaskBindToProjectRequest(testToken, NULL_PROJECT_ID, taskId)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.bindTaskToProject(
                        new TaskBindToProjectRequest(testToken, testProjectId, NULL_TASK_ID)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.bindTaskToProject(
                        new TaskBindToProjectRequest(testToken, testProjectId, BAD_TASK_ID)
                )
        );
        @NotNull final TaskBindToProjectRequest taskBindRequest =
                new TaskBindToProjectRequest(testToken, testProjectId, taskId);
        @NotNull final TaskBindToProjectResponse taskBindResponse =
                projectTaskEndpoint.bindTaskToProject(taskBindRequest);
        Assert.assertNotNull(taskBindResponse);
        @NotNull final TaskGetByIdRequest taskGetByIdRequest = new TaskGetByIdRequest(testToken, taskId);
        @NotNull final TaskGetByIdResponse taskGetByIdResponse = taskEndpoint.getTaskById(taskGetByIdRequest);
        Assert.assertNotNull(taskGetByIdResponse);
        Assert.assertNotNull(taskGetByIdResponse.getTask());
        Assert.assertEquals(taskGetByIdResponse.getTask().getProjectId(), testProjectId);
    }

    @Test
    public void unbindTaskToProject() {
        Assert.assertNotNull(testTasksList);
        @NotNull final TaskDTO task = testTasksList.get(0);
        @NotNull final String taskId = task.getId();
        @NotNull final TaskBindToProjectRequest taskBindRequest =
                new TaskBindToProjectRequest(testToken, testProjectId, taskId);
        projectTaskEndpoint.bindTaskToProject(taskBindRequest);
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.unbindTaskFromProject(new TaskUnbindFromProjectRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.unbindTaskFromProject(new TaskUnbindFromProjectRequest(NULL_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.unbindTaskFromProject(new TaskUnbindFromProjectRequest(BAD_TOKEN))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.unbindTaskFromProject(new TaskUnbindFromProjectRequest(adminToken))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.unbindTaskFromProject(new TaskUnbindFromProjectRequest(testToken))
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.unbindTaskFromProject(
                        new TaskUnbindFromProjectRequest(testToken, NULL_PROJECT_ID, taskId)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.unbindTaskFromProject(
                        new TaskUnbindFromProjectRequest(testToken, testProjectId, NULL_TASK_ID)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> projectTaskEndpoint.unbindTaskFromProject(
                        new TaskUnbindFromProjectRequest(testToken, testProjectId, BAD_TASK_ID)
                )
        );
        @NotNull final TaskUnbindFromProjectRequest taskUnbindRequest =
                new TaskUnbindFromProjectRequest(testToken, testProjectId, taskId);
        @NotNull final TaskUnbindFromProjectResponse taskUnbindResponse =
                projectTaskEndpoint.unbindTaskFromProject(taskUnbindRequest);
        Assert.assertNotNull(taskUnbindResponse);
        @NotNull final TaskGetByIdRequest taskGetByIdRequest = new TaskGetByIdRequest(testToken, taskId);
        @NotNull final TaskGetByIdResponse taskGetByIdResponse = taskEndpoint.getTaskById(taskGetByIdRequest);
        Assert.assertNotNull(taskGetByIdResponse);
        Assert.assertNotNull(taskGetByIdResponse.getTask());
        Assert.assertNotEquals(taskGetByIdResponse.getTask().getProjectId(), testProjectId);
    }

    private void createManyTestTasks() {
        for (int i = 0; i < COUNT_TEST_TASKS; i++) {
            @NotNull final TaskCreateRequest createRequest = new TaskCreateRequest(testToken);
            createRequest.setName(TASK_NAME);
            createRequest.setDescription(TASK_DESCRIPTION);
            taskEndpoint.createTask(createRequest);
        }
    }

    @NotNull
    private String createTestProject() {
        if (testProjectId == null) {
            @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(testToken);
            projectCreateRequest.setName(PROJECT_NAME);
            projectCreateRequest.setDescription(PROJECT_DESCRIPTION);
            @NotNull final ProjectCreateResponse projectCreateResponse = projectEndpoint.
                    createProject(projectCreateRequest);
            return projectCreateResponse.getProject().getId();
        } else {
            return testProjectId;
        }
    }

    private void removeTestProject(@NotNull final String testProjectId) {
        @NotNull final ProjectRemoveByIdRequest removeByIdRequest = new ProjectRemoveByIdRequest(testToken);
        removeByIdRequest.setId(testProjectId);
        projectEndpoint.removeProjectById(removeByIdRequest);
    }

}
