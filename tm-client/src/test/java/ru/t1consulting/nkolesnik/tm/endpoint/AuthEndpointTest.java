package ru.t1consulting.nkolesnik.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IAuthEndpoint;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserLoginRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserLogoutRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserProfileRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.user.UserLoginResponse;
import ru.t1consulting.nkolesnik.tm.dto.response.user.UserLogoutResponse;
import ru.t1consulting.nkolesnik.tm.dto.response.user.UserProfileResponse;
import ru.t1consulting.nkolesnik.tm.marker.SoapCategory;

import java.util.UUID;

@Category(SoapCategory.class)
public class AuthEndpointTest {

    @NotNull
    private static final String USER_LOGIN = "admin";
    @NotNull
    private static final String USER_PASSWORD = "admin";
    @NotNull
    private static final String BAD_USER_LOGIN = "admin1";
    @NotNull
    private static final String BAD_USER_PASSWORD = "admin1";
    @NotNull
    private static final String BAD_TOKEN = UUID.randomUUID().toString();
    @Nullable
    private static final String NULL_LOGIN = null;
    @Nullable
    private static final String NULL_PASSWORD = null;
    @NotNull
    private final IAuthEndpoint endpoint = IAuthEndpoint.newInstance();

    @Test
    public void login() {
        Assert.assertThrows(Exception.class, () -> endpoint.login(new UserLoginRequest()));
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(USER_LOGIN, USER_PASSWORD);
        @NotNull final UserLoginResponse response = endpoint.login(loginRequest);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getToken());
        Assert.assertTrue(response.getSuccess());
        Assert.assertThrows(
                Exception.class,
                () -> endpoint.login(new UserLoginRequest(BAD_USER_LOGIN, BAD_USER_PASSWORD))
        );
        Assert.assertThrows(
                Exception.class,
                () -> endpoint.login(new UserLoginRequest(USER_LOGIN, BAD_USER_PASSWORD))
        );
        Assert.assertThrows(Exception.class, () -> endpoint.login(new UserLoginRequest(NULL_LOGIN, USER_PASSWORD)));
        Assert.assertThrows(Exception.class, () -> endpoint.login(new UserLoginRequest(USER_LOGIN, NULL_PASSWORD)));
        Assert.assertThrows(Exception.class, () -> endpoint.login(new UserLoginRequest(NULL_LOGIN, NULL_PASSWORD)));
    }

    @Test
    public void logout() {
        Assert.assertThrows(Exception.class, () -> endpoint.logout(new UserLogoutRequest()));
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(USER_LOGIN, USER_PASSWORD);
        @NotNull final UserLoginResponse response = endpoint.login(loginRequest);
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(response.getToken());
        @NotNull final UserLogoutResponse logoutResponse = endpoint.logout(logoutRequest);
        Assert.assertNotNull(logoutResponse);
        Assert.assertTrue(logoutResponse.getSuccess());
        Assert.assertThrows(Exception.class, () -> endpoint.logout(new UserLogoutRequest(BAD_TOKEN)));
    }

    @Test
    public void profile() {
        Assert.assertThrows(Exception.class, () -> endpoint.profile(new UserProfileRequest()));
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(USER_LOGIN, USER_PASSWORD);
        @NotNull final UserLoginResponse loginResponse = endpoint.login(loginRequest);
        @NotNull final UserProfileRequest profileRequest = new UserProfileRequest(loginResponse.getToken());
        @NotNull final UserProfileResponse profileResponse = endpoint.profile(profileRequest);
        Assert.assertNotNull(profileResponse);
        Assert.assertNotNull(profileRequest.getToken());
        Assert.assertNotNull(profileResponse.getUser());
        Assert.assertThrows(Exception.class, () -> endpoint.profile(new UserProfileRequest(BAD_TOKEN)));
    }

}
