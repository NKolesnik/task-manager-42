package ru.t1consulting.nkolesnik.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.mybatis.dynamic.sql.SortSpecification;
import org.mybatis.dynamic.sql.render.RenderingStrategy;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import ru.t1consulting.nkolesnik.tm.api.repository.ITaskRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.IProjectService;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.comparator.CreatedComparator;
import ru.t1consulting.nkolesnik.tm.comparator.DateBeginComparator;
import ru.t1consulting.nkolesnik.tm.comparator.StatusComparator;
import ru.t1consulting.nkolesnik.tm.dto.model.ProjectDTO;
import ru.t1consulting.nkolesnik.tm.dto.model.TaskDTO;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDTO;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.marker.DataCategory;
import ru.t1consulting.nkolesnik.tm.model.TaskProvider;
import ru.t1consulting.nkolesnik.tm.service.ConnectionService;
import ru.t1consulting.nkolesnik.tm.service.ProjectService;
import ru.t1consulting.nkolesnik.tm.service.PropertyService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

import static org.mybatis.dynamic.sql.SqlBuilder.*;
import static ru.t1consulting.nkolesnik.tm.model.TaskProvider.*;

@Category(DataCategory.class)
public class TaskRepositoryTest {

    @NotNull
    private static final String TASK_NAME_PREFIX = "TEST_TASK_NAME";

    @NotNull
    private static final String TASK_DESCRIPTION_PREFIX = "TEST_TASK_DESCRIPTION";

    @Nullable
    private static final String NULL_TASK_ID = null;

    @Nullable
    private static final String NULL_USER_ID = null;

    @Nullable
    private static final String BAD_USER_ID = UUID.randomUUID().toString();

    @Nullable
    private static final String BAD_TASK_ID = UUID.randomUUID().toString();

    @Nullable
    private static final TaskDTO NULL_TASK = null;

    private static final long REPOSITORY_SIZE = 1000L;

    private static final long EMPTY_REPOSITORY_SIZE = 0L;

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private static String TEST_USER_ID = "";
    @NotNull
    private static String projectId = "";
    @NotNull
    private final String taskId = UUID.randomUUID().toString();
    @NotNull
    private List<TaskDTO> taskList;

    @NotNull
    private TaskDTO task;

    @BeforeClass
    public static void prepareTestEnvironment() {
        getTestData();
        saveDbBackup();
    }

    @AfterClass
    public static void restoreProdEnvironment() {
        loadDbBackup();
    }

    @SneakyThrows
    public static void saveDbBackup() {
        @NotNull final String userName = propertyService.getDatabaseUsername();
        @NotNull final String userPassword = propertyService.getDatabasePassword();
        @NotNull final String url = propertyService.getDatabaseConnectionString();
        @NotNull final Connection connection = DriverManager.getConnection(url, userName, userPassword);
        connection.setAutoCommit(false);
        String sqlSaveData = "SELECT * INTO \"backup_tasks\" FROM \"tasks\";";
        String sqlClearTable = "DELETE FROM \"tasks\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlSaveData);
            statement.executeUpdate(sqlClearTable);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    public static void loadDbBackup() {
        @NotNull final String userName = propertyService.getDatabaseUsername();
        @NotNull final String userPassword = propertyService.getDatabasePassword();
        @NotNull final String url = propertyService.getDatabaseConnectionString();
        @NotNull final Connection connection = DriverManager.getConnection(url, userName, userPassword);
        connection.setAutoCommit(false);
        String sqlClearTable = "DELETE FROM \"tasks\";";
        String sqlLoadData = "INSERT INTO \"tasks\" (SELECT * FROM \"backup_tasks\");";
        String sqlDropBackupTable = "DROP TABLE \"backup_tasks\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlClearTable);
            statement.executeUpdate(sqlLoadData);
            statement.executeUpdate(sqlDropBackupTable);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    public static void getTestData() {
        @NotNull final String userName = propertyService.getDatabaseUsername();
        @NotNull final String userPassword = propertyService.getDatabasePassword();
        @NotNull final String url = propertyService.getDatabaseConnectionString();
        @NotNull final Connection connection = DriverManager.getConnection(url, userName, userPassword);
        connection.setAutoCommit(false);
        String sqlGetTestUserIdData = "SELECT id FROM \"users\" WHERE \"login\" ='test';";
        try {
            Statement statement = connection.createStatement();
            @NotNull final ResultSet userSet = statement.executeQuery(sqlGetTestUserIdData);
            if (userSet.next()) {
                TEST_USER_ID = userSet.getString(1);
            }
            String sqlGetTestProjectIdData = "SELECT id FROM \"projects\" WHERE \"user_id\" = '" + TEST_USER_ID + "';";
            @NotNull final ResultSet projectSet = statement.executeQuery(sqlGetTestProjectIdData);
            if (projectSet.next()) {
                projectId = projectSet.getString(1);
            }
            if (projectId == null || projectId.isEmpty()) {
                @NotNull final ProjectDTO project = createTestProject();
                projectService.add(project);
                projectId = project.getId();
            }
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    private static ProjectDTO createTestProject() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(TEST_USER_ID);
        project.setName("TEST_PROJ");
        project.setDescription("TEST");
        return project;
    }

    @Before
    public void setup() {
        task = createOneTask();
        taskList = createManyTasks();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            repository.clear();
        }
    }

    @After
    public void cleanup() {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            repository.clear();
        }
    }

    @Test
    public void add() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            Assert.assertThrows(Exception.class, () -> repository.add(NULL_TASK));
            session.rollback();
            Assert.assertThrows(Exception.class, () -> repository.addForUser(NULL_USER_ID, NULL_TASK));
            session.rollback();
            Assert.assertThrows(Exception.class, () -> repository.addForUser(BAD_USER_ID, createOneTask()));
            session.rollback();
            @NotNull final TaskDTO task = new TaskDTO();
            task.setUserId(TEST_USER_ID);
            repository.clear();
            repository.add(task);
            Assert.assertEquals(1, repository.getSize());
            session.commit();
            @NotNull final UserDTO user = new UserDTO();
            @NotNull final TaskDTO taskWithUser = new TaskDTO();
            repository.clear();
            repository.addForUser(TEST_USER_ID, taskWithUser);
            Assert.assertEquals(1, repository.getSize());
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void getSize() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
            session.commit();
            for (TaskDTO task : taskList)
                repository.add(task);
            Assert.assertEquals(REPOSITORY_SIZE, repository.getSize());
            session.commit();
            repository.clear();
            session.commit();
            Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSizeForUser(TEST_USER_ID));
            session.commit();
            for (TaskDTO task : taskList)
                repository.addForUser(TEST_USER_ID, task);
            Assert.assertEquals(REPOSITORY_SIZE, repository.getSizeForUser(TEST_USER_ID));
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void findAll() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            for (TaskDTO task : taskList)
                repository.add(task);
            @Nullable List<TaskDTO> repositoryTasks = repository.findAll();
            Assert.assertNotNull(repositoryTasks);
            Assert.assertEquals(REPOSITORY_SIZE, repositoryTasks.size());
            session.commit();
            repository.clear();
            session.commit();
            repositoryTasks = repository.findAllForUser(TEST_USER_ID);
            Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repositoryTasks.size());
            session.commit();
            for (TaskDTO task : taskList) {
                repository.addForUser(TEST_USER_ID, task);
            }
            repositoryTasks = repository.findAllForUser(TEST_USER_ID);
            Assert.assertEquals(REPOSITORY_SIZE, repositoryTasks.size());
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void findAllWithCriteria() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            for (TaskDTO task : taskList)
                repository.add(task);

            @NotNull SelectStatementProvider statement =
                    select(id, TaskProvider.userId, name, description, status, created, dateBegin, dateEnd).
                            from(tasks).
                            orderBy(getSortSpec(CreatedComparator.INSTANCE)).
                            build().
                            render(RenderingStrategy.MYBATIS3);
            @Nullable List<TaskDTO> repositoryTasks = repository.findAllWithCriteria(statement);
            Assert.assertNotNull(repositoryTasks);
            Assert.assertEquals(REPOSITORY_SIZE, repositoryTasks.size());
            session.commit();
            repository.clear();
            session.commit();
            statement =
                    select(id, TaskProvider.userId, name, description, status, created, dateBegin, dateEnd).
                            from(tasks).
                            where(TaskProvider.userId, isEqualTo(TEST_USER_ID)).
                            orderBy(getSortSpec(CreatedComparator.INSTANCE)).
                            build().
                            render(RenderingStrategy.MYBATIS3);
            repositoryTasks = repository.findAllWithCriteria(statement);
            Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repositoryTasks.size());
            session.commit();
            for (TaskDTO task : taskList) {
                repository.addForUser(TEST_USER_ID, task);
            }
            repositoryTasks = repository.findAllWithCriteria(statement);
            Assert.assertEquals(REPOSITORY_SIZE, repositoryTasks.size());
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void findById() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            repository.add(task);
            Assert.assertNull(repository.findById(NULL_TASK_ID));
            session.commit();
            Assert.assertNull(repository.findById(BAD_TASK_ID));
            session.commit();
            Assert.assertNull(repository.findByIdForUser(NULL_USER_ID, taskId));
            session.commit();
            Assert.assertNull(repository.findByIdForUser(BAD_USER_ID, taskId));
            session.commit();
            @Nullable TaskDTO repositoryTask = repository.findById(taskId);
            Assert.assertNotNull(repositoryTask);
            Assert.assertEquals(taskId, repositoryTask.getId());
            session.commit();
            repositoryTask = repository.findByIdForUser(TEST_USER_ID, taskId);
            Assert.assertNotNull(repositoryTask);
            Assert.assertEquals(taskId, repositoryTask.getId());
            session.commit();
            for (TaskDTO task : taskList) {
                repository.addForUser(TEST_USER_ID, task);
            }
            repositoryTask = repository.findByIdForUser(TEST_USER_ID, taskId);
            Assert.assertNotNull(repositoryTask);
            Assert.assertEquals(taskId, repositoryTask.getId());
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void findAllByProjectId() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            repository.add(task);
            Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.findAllByProjectId(NULL_USER_ID, projectId).size());
            session.commit();
            Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.findAllByProjectId(BAD_USER_ID, projectId).size());
            session.commit();
            @Nullable List<TaskDTO> repositoryTasks = repository.findAllByProjectId(TEST_USER_ID, projectId);
            Assert.assertNotNull(repositoryTasks);
            Assert.assertNotNull(repositoryTasks.get(0));
            Assert.assertEquals(taskId, repositoryTasks.get(0).getId());
            session.commit();
            repository.clear();
            session.commit();
            for (TaskDTO task : taskList) {
                repository.addForUser(TEST_USER_ID, task);
            }
            repositoryTasks = repository.findAllByProjectId(TEST_USER_ID, projectId);
            Assert.assertNotNull(repositoryTasks);
            Assert.assertEquals(REPOSITORY_SIZE, repositoryTasks.size());
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void existsById() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            repository.add(task);
            session.commit();
            Assert.assertFalse(repository.existsById(NULL_TASK_ID));
            Assert.assertFalse(repository.existsById(BAD_TASK_ID));
            Assert.assertFalse(repository.existsByIdForUser(NULL_USER_ID, taskId));
            Assert.assertFalse(repository.existsByIdForUser(BAD_USER_ID, taskId));
            Assert.assertTrue(repository.existsById(taskId));
            Assert.assertTrue(repository.existsByIdForUser(TEST_USER_ID, taskId));
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void updateTask() {
        @NotNull final String newName = "NEW TASK NAME";
        @NotNull final String newDescription = "NEW TASK DESCRIPTION";
        @NotNull final Status newStatus = Status.COMPLETED;
        @NotNull final Date newDateEnd = new Date();
        @Nullable final String oldName;
        @Nullable final String oldDescription;
        @Nullable final Status oldStatus;
        @Nullable final Date oldDateEnd;
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            repository.add(task);
            session.commit();
            oldName = task.getName();
            oldDescription = task.getDescription();
            oldStatus = task.getStatus();
            oldDateEnd = task.getDateEnd();
            @NotNull UpdateStatementProvider statement =
                    update(tasks).
                            set(name).equalTo(newName).
                            set(description).equalTo(newDescription).
                            set(status).equalTo(newStatus).
                            set(dateEnd).equalTo(newDateEnd).
                            where(id, isEqualTo(taskId)).
                            build().render(RenderingStrategy.MYBATIS3);
            repository.update(statement);
            session.commit();
            @Nullable final TaskDTO repositoryTask = repository.findById(taskId);
            Assert.assertNotNull(repositoryTask);
            Assert.assertEquals(newName, repositoryTask.getName());
            Assert.assertEquals(newDescription, repositoryTask.getDescription());
            Assert.assertEquals(newStatus, repositoryTask.getStatus());
            Assert.assertEquals(newDateEnd, repositoryTask.getDateEnd());
            Assert.assertNotEquals(oldName, repositoryTask.getName());
            Assert.assertNotEquals(oldDescription, repositoryTask.getDescription());
            Assert.assertNotEquals(oldStatus, repositoryTask.getStatus());
            Assert.assertNotEquals(oldDateEnd, repositoryTask.getDateEnd());
            statement =
                    update(tasks).
                            set(name).equalTo(oldName).
                            set(description).equalTo(oldDescription).
                            set(status).equalTo(oldStatus).
                            set(dateEnd).equalTo(oldDateEnd).
                            where(id, isEqualTo(taskId)).
                            build().render(RenderingStrategy.MYBATIS3);
            repository.update(statement);
            session.commit();
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void clear() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            for (TaskDTO task : taskList) {
                repository.addForUser(TEST_USER_ID, task);
            }
            session.commit();
            Assert.assertEquals(REPOSITORY_SIZE, repository.getSize());
            repository.clear();
            Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
            session.commit();
            for (TaskDTO task : taskList) {
                repository.addForUser(TEST_USER_ID, task);
            }
            session.commit();
            Assert.assertEquals(REPOSITORY_SIZE, repository.getSizeForUser(TEST_USER_ID));
            repository.clear();
            Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSizeForUser(TEST_USER_ID));
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void remove() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            repository.add(task);
            session.commit();
            Assert.assertEquals(1, repository.getSize());
            repository.remove(task);
            Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
            session.commit();
            repository.addForUser(TEST_USER_ID, task);
            session.commit();
            Assert.assertEquals(1, repository.getSize());
            repository.removeForUser(TEST_USER_ID, task);
            Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void removeById() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            repository.add(task);
            session.commit();
            Assert.assertEquals(1, repository.getSize());
            repository.removeById(taskId);
            Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
            session.commit();
            repository.addForUser(TEST_USER_ID, task);
            session.commit();
            Assert.assertEquals(1, repository.getSize());
            repository.removeByIdForUser(TEST_USER_ID, taskId);
            Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void removeByProjectId() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            repository.add(task);
            session.commit();
            @Nullable TaskDTO repositoryTask = repository.findById(taskId);
            Assert.assertNotNull(repositoryTask);
            Assert.assertEquals(taskId, repositoryTask.getId());
            session.commit();
            repository.removeByProjectId(task.getUserId(), projectId);
            session.commit();
            repositoryTask = repository.findById(taskId);
            Assert.assertNull(repositoryTask);
            repository.clear();
            session.commit();
            for (TaskDTO task : taskList) {
                repository.addForUser(TEST_USER_ID, task);
            }
            session.commit();
            @Nullable List<TaskDTO> repositoryTasks = repository.findAllByProjectId(TEST_USER_ID, projectId);
            Assert.assertNotNull(repositoryTasks);
            Assert.assertEquals(REPOSITORY_SIZE, repositoryTasks.size());
            repository.removeByProjectId(task.getUserId(), projectId);
            session.commit();
            repositoryTasks = repository.findAllByProjectId(TEST_USER_ID, projectId);
            Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repositoryTasks.size());
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @NotNull
    private TaskDTO createOneTask() {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setId(taskId);
        task.setUserId(TEST_USER_ID);
        task.setName(TASK_NAME_PREFIX);
        task.setDescription(TASK_DESCRIPTION_PREFIX);
        task.setStatus(Status.IN_PROGRESS);
        task.setProjectId(projectId);
        return task;
    }

    @NotNull
    private List<TaskDTO> createManyTasks() {
        @NotNull final List<TaskDTO> tasks = new ArrayList<>();
        for (int i = 0; i < REPOSITORY_SIZE; i++) {
            TaskDTO task = new TaskDTO();
            task.setName(TASK_NAME_PREFIX + i);
            task.setDescription(TASK_DESCRIPTION_PREFIX + i);
            task.setUserId(TEST_USER_ID);
            task.setProjectId(projectId);
            tasks.add(task);
        }
        return tasks;
    }

    @NotNull
    protected SortSpecification getSortSpec(@NotNull Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return created;
        else if (comparator == StatusComparator.INSTANCE) return status;
        else if (comparator == DateBeginComparator.INSTANCE) return dateBegin;
        else return name;
    }


}
