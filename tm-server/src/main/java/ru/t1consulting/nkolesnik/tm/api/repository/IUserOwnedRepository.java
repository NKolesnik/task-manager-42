package ru.t1consulting.nkolesnik.tm.api.repository;

import ru.t1consulting.nkolesnik.tm.dto.model.AbstractUserOwnedModelDTO;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModelDTO> extends IRepository<M> {

}
