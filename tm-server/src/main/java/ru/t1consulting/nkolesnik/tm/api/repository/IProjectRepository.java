package ru.t1consulting.nkolesnik.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import ru.t1consulting.nkolesnik.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<ProjectDTO> {

    @Insert("INSERT INTO projects (id, created, name, description, status, user_id, date_begin, date_end)"
            + "VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId}, #{dateBegin}, #{dateEnd});")
    void add(@Nullable ProjectDTO project);

    @Insert("INSERT INTO projects (id, created, name, description, status, user_id, date_begin, date_end)"
            + "VALUES (#{project.id}, #{project.created}, #{project.name}," +
            " #{project.description}, #{project.status}, #{userId}," +
            " #{project.dateBegin}, #{project.dateEnd});")
    void addForUser(@Param("userId") @Nullable String userId, @Param("project") @Nullable ProjectDTO project);

    @Select("SELECT COUNT(1) FROM projects;")
    long getSize();

    @Select("SELECT COUNT(1) FROM projects WHERE user_id = #{userId};")
    long getSizeForUser(@Param("userId") @Nullable String userId);

    @Nullable
    @Select("SELECT id, user_id, name, description, status, date_begin, date_end, created FROM projects;")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    List<ProjectDTO> findAll();

    @Nullable
    @Select("SELECT id, user_id, name, description, status, date_begin, date_end, created " +
            "FROM projects where user_id = #{userId};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    List<ProjectDTO> findAllForUser(@Param("userId") @Nullable String userId);

    @Nullable
    @SelectProvider(type = SqlProviderAdapter.class, method = "select")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    List<ProjectDTO> findAllWithCriteria(@Nullable SelectStatementProvider statement);

    @Nullable
    @Select("SELECT id, user_id, name, description, status, date_begin, date_end, created " +
            "FROM projects WHERE id = #{id};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    ProjectDTO findById(@Param("id") @Nullable String id);

    @Nullable
    @Select("SELECT id, user_id, name, description, status, date_begin, date_end, created " +
            "FROM projects WHERE id = #{id} AND user_id = #{userId};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    ProjectDTO findByIdForUser(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Select("SELECT COUNT(1)=1 FROM projects WHERE Id = #{id};")
    boolean existsById(@Param("id") @Nullable String id);

    @Select("SELECT COUNT(1)=1 FROM projects WHERE Id = #{id} AND user_id = #{userId};")
    boolean existsByIdForUser(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @UpdateProvider(type = SqlProviderAdapter.class, method = "update")
    void update(@NotNull UpdateStatementProvider statement);

    @Delete("DELETE FROM projects;")
    void clear();

    @Delete("DELETE FROM projects WHERE user_id = #{userId};")
    void clearForUser(@Param("userId") @Nullable String userId);

    @Delete("DELETE FROM projects WHERE id = #{project.id};")
    void remove(@Param("project") @Nullable ProjectDTO project);

    @Delete("DELETE FROM projects WHERE id = #{project.id} AND user_id = #{userId};")
    void removeForUser(@Param("userId") @Nullable String userId, @Param("project") @Nullable ProjectDTO project);

    @Delete("DELETE FROM projects WHERE id = #{id};")
    void removeById(@Param("id") @Nullable String id);

    @Delete("DELETE FROM projects WHERE id = #{id} AND user_id = #{userId};")
    void removeByIdForUser(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

}
