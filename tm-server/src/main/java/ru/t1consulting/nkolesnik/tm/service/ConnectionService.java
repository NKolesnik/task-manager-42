package ru.t1consulting.nkolesnik.tm.service;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.repository.IProjectRepository;
import ru.t1consulting.nkolesnik.tm.api.repository.ITaskRepository;
import ru.t1consulting.nkolesnik.tm.api.repository.IUserRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.dto.model.ProjectDTO;
import ru.t1consulting.nkolesnik.tm.dto.model.TaskDTO;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDTO;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.model.Task;
import ru.t1consulting.nkolesnik.tm.model.User;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        this.entityManagerFactory = getEntityManagerFactory();
    }

    @NotNull
    @Override
    public SqlSessionFactory getSqlSessionFactory() {
        @NotNull final String userName = propertyService.getDatabaseUsername();
        @NotNull final String userPassword = propertyService.getDatabasePassword();
        @NotNull final String url = propertyService.getDatabaseConnectionString();
        @NotNull final String driver = propertyService.getDatabaseDriver();
        @NotNull final DataSource dataSource = new PooledDataSource(driver, url, userName, userPassword);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("development", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(ITaskRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(IUserRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

    @NotNull
    @Override
    public SqlSession getSqlSession() {
        return getSqlSessionFactory().openSession();
    }

    @NotNull
    @Override
    public EntityManagerFactory getEntityManagerFactory() {
        @NotNull final Map<String, String> properties = new HashMap<>();
        properties.put(org.hibernate.cfg.Environment.DRIVER, propertyService.getDatabaseDriver());
        properties.put(org.hibernate.cfg.Environment.URL, propertyService.getDatabaseConnectionString());
        properties.put(org.hibernate.cfg.Environment.USER, propertyService.getDatabaseUsername());
        properties.put(org.hibernate.cfg.Environment.PASS, propertyService.getDatabasePassword());
        properties.put(org.hibernate.cfg.Environment.DIALECT, propertyService.getDatabaseDialect());
        properties.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, propertyService.getDatabaseHbm2Ddl());
        properties.put(org.hibernate.cfg.Environment.SHOW_SQL, propertyService.getDatabaseShowSql());
        properties.put(org.hibernate.cfg.Environment.FORMAT_SQL, propertyService.getDatabaseFormatSql());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(properties);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources metadataSources = new MetadataSources(registry);
        metadataSources.addAnnotatedClass(UserDTO.class);
        metadataSources.addAnnotatedClass(ProjectDTO.class);
        metadataSources.addAnnotatedClass(TaskDTO.class);
        metadataSources.addAnnotatedClass(User.class);
        metadataSources.addAnnotatedClass(Project.class);
        metadataSources.addAnnotatedClass(Task.class);
        @NotNull final Metadata metadata = metadataSources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();

    }

}

